require 'rails_helper'

describe ApplicationHelper, :type => :helper do
  describe "#show_error_messages" do
    it "should return one message without comma" do
      expect(helper.show_error_messages(["some message"])).to eq('some message')
    end
    it "should return two or more messages without comma in the end" do
      expect(helper.show_error_messages(["some message, other message"])).to eq('some message, other message')
    end
  end

  describe "#set_classes_for_input" do
    it "should return 'validate invalid' if param is true" do
      expect(helper.set_classes_for_input(true)).to eq('validate invalid')
    end

    it "should return 'validate' if param is false" do
      expect(helper.set_classes_for_input(false)).to eq('validate')
    end
  end
end
