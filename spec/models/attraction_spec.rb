require 'rails_helper'

RSpec.describe Attraction, :type => :model do
  it 'should require name' do
    attraction = FactoryGirl.build(:attraction, name: nil)
    expect(attraction).not_to be_valid
    expect(attraction.errors_on(:name)).to eq(["não pode ficar em branco"])
  end

  it 'should require due_date' do
    attraction = FactoryGirl.build(:attraction, due_date: nil)
    expect(attraction).not_to be_valid
    expect(attraction.errors_on(:due_date)).to eq(["não pode ficar em branco"])
  end

  it 'should require user' do
    attraction = FactoryGirl.build(:attraction, user: nil)
    expect(attraction).not_to be_valid
    expect(attraction.errors_on(:user)).to eq(["não pode ficar em branco"])
  end

  it 'should require due_time' do
    attraction = FactoryGirl.build(:attraction, due_time: nil)
    expect(attraction).not_to be_valid
    expect(attraction.errors_on(:due_time)).to eq(["não pode ficar em branco"])
  end
end
