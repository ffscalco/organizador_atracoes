require 'rails_helper'

RSpec.describe User, :type => :model do
  describe "#admin?" do
    it 'should return true if user is admin' do
      user = FactoryGirl.build(:user, admin: true)

      expect(user.admin?).to eq(true)
    end

    it 'should return false if user is no admin' do
      user = FactoryGirl.build(:user)

      expect(user.admin?).to eq(false)
    end
  end
end
