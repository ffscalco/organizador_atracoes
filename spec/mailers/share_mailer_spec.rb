require "rails_helper"

RSpec.describe ShareMailer, :type => :mailer do
  let(:user) { FactoryGirl.create(:user) }
  let(:attraction) { FactoryGirl.create(:attraction, user: user) }

  describe 'share' do
    let(:mail) { ShareMailer.share(user.id, [attraction.id]) }

    it 'renders the subject' do
      expect(mail.subject).to eql('Novas atrações foram compartilhadas com você')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eql([user.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eql(['ffscalco28@gmail.com'])
    end

    it 'assigns @user' do
      expect(mail.body.encoded).to match(user.email[/[^@]+/])
    end

    it 'assigns @attractions' do
      expect(mail.body.encoded).to match(attraction.name)
    end
  end
end
