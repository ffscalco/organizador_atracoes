require 'rails_helper'

RSpec.describe AttractionsController, :type => :controller do
  let(:user) {FactoryGirl.create(:user, email: "attraction@email.com")}
  let(:valid_attributes) {
    {
      name: "Some name",
      midia: "Some midia",
      due_date_submit: Date.today,
      due_time: Time.now
    }
  }

  let(:invalid_attributes) {
    {
      some_attribute: "some value",
      name: "",
      midia: "Some midia",
      due_date: nil,
      due_time: nil
    }
  }

  let(:valid_session) { {} }

  context "when not logged in" do
    let(:attraction) {FactoryGirl.create(:attraction, user: user)}

    it "should redirect to new user session when try to access index" do
      get :index, {}, valid_session
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should redirect to new user session when try to access new" do
      get :new, {}, valid_session
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should redirect to new user session when try to access edit" do
      get :edit, {:id => attraction.to_param}, valid_session
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should redirect to new user session when try to access create" do
      post :create, {:attraction => valid_attributes}, valid_session
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should redirect to new user session when try to access update" do
      put :update, {:id => attraction.to_param, :attraction => valid_attributes}, valid_session

      expect(response).to redirect_to(new_user_session_path)
    end

    it "should redirect to new user session when try to access destroy" do
      delete :destroy, {:id => attraction.to_param}, valid_session

      expect(response).to redirect_to(new_user_session_path)
    end
  end

  context "when logged in" do
    before :each do
      sign_in(user)
    end

    describe "GET index" do
      it "assigns all attractions of the user as @attractions" do
        attraction = FactoryGirl.create(:attraction, user: user)
        FactoryGirl.create(:attraction)

        expect(Attraction.all.size).to eq(2)
        get :index, {}, valid_session
        expect(assigns(:attractions)).to eq([attraction])
      end

      it "assigns all attractions of the user as @attractions in order of datetime" do
        attraction1 = FactoryGirl.create(:attraction, user: user, due_date: "2015-01-27", due_time: "03:30:00")
        attraction2 = FactoryGirl.create(:attraction, user: user, due_date: "2015-01-26", due_time: "04:30:00")
        attraction3 = FactoryGirl.create(:attraction, user: user, due_date: "2015-01-27", due_time: "05:30:00")
        attraction4 = FactoryGirl.create(:attraction, user: user, due_date: "2015-01-27", due_time: "04:00:00")
        attraction5 = FactoryGirl.create(:attraction, due_date: "27/01/2015", due_time: "06:00:00")

        time_now = Time.parse("2015-01-27 06:30:00")
        allow(Time).to receive(:now).and_return(time_now)

        expect(Attraction.all.size).to eq(5)
        get :index, {}, valid_session
        expect(assigns(:attractions)).to eq([attraction3, attraction4, attraction1, attraction2])
      end
    end

    describe "GET new" do
      it "assigns a new attraction as @attraction" do
        get :new, {}, valid_session
        expect(assigns(:attraction)).to be_a_new(Attraction)
      end
    end

    describe "GET edit" do
      it "assigns the requested attraction as @attraction" do
        attraction = FactoryGirl.create(:attraction, user: user)
        get :edit, {:id => attraction.to_param}, valid_session
        attraction.reload
        expect(assigns(:attraction)).to eq(attraction)
      end
    end

    describe "POST create" do
      describe "with valid params" do
        it "creates a new Attraction" do
          expect {
            post :create, {:attraction => valid_attributes}, valid_session
          }.to change(Attraction, :count).by(1)
        end

        it "assigns a newly created attraction as @attraction" do
          post :create, {:attraction => valid_attributes}, valid_session
          expect(assigns(:attraction)).to be_a(Attraction)
          expect(assigns(:attraction)).to be_persisted
        end

        it "redirects to the attractions index" do
          post :create, {:attraction => valid_attributes}, valid_session
          expect(response).to redirect_to(attractions_path)
        end

        it "should have flash message" do
          post :create, {:attraction => valid_attributes}, valid_session

          expect(flash[:toast]).to be_present
          expect(flash[:toast]).to eq("Atração criada com sucesso!")
        end
      end

      describe "with invalid params" do
        it "assigns a newly created but unsaved attraction as @attraction" do
          post :create, {:attraction => invalid_attributes}, valid_session
          expect(assigns(:attraction)).to be_a_new(Attraction)
        end

        it "re-renders the 'new' template" do
          post :create, {:attraction => invalid_attributes}, valid_session
          expect(response).to render_template("new")
        end

        it "should have flash message" do
          post :create, {:attraction => invalid_attributes}, valid_session

          expect(flash[:toast]).to be_present
          expect(flash[:toast]).to eq("Houve problemas ao criar uma nova atração.")
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        let(:new_attributes) {
          {
            name: "Some other name",
            midia: "Some other midia"
          }
        }

        it "updates the requested attraction" do
          attraction = FactoryGirl.create(:attraction, user: user)
          put :update, {:id => attraction.to_param, :attraction => new_attributes}, valid_session
          attraction.reload
          expect(assigns(:attraction).name).to eq(attraction.name)
        end

        it "assigns the requested attraction as @attraction" do
          attraction = FactoryGirl.create(:attraction, user: user)
          put :update, {:id => attraction.to_param, :attraction => valid_attributes}, valid_session
          expect(assigns(:attraction)).to eq(attraction)
        end

        it "redirects to the attractions index" do
          attraction = FactoryGirl.create(:attraction, user: user)
          put :update, {:id => attraction.to_param, :attraction => valid_attributes}, valid_session
          expect(response).to redirect_to(attractions_path)
        end

        it "should have flash message" do
          attraction = FactoryGirl.create(:attraction, user: user)
          put :update, {:id => attraction.to_param, :attraction => valid_attributes}, valid_session
          expect(flash[:toast]).to be_present
          expect(flash[:toast]).to eq("Atração atualizada com sucesso!")
        end
      end

      describe "with invalid params" do
        it "assigns the attraction as @attraction" do
          attraction = FactoryGirl.create(:attraction, user: user)
          put :update, {:id => attraction.to_param, :attraction => invalid_attributes}, valid_session
          expect(assigns(:attraction)).to eq(attraction)
        end

        it "re-renders the 'edit' template" do
          attraction = FactoryGirl.create(:attraction, user: user)
          put :update, {:id => attraction.to_param, :attraction => invalid_attributes}, valid_session
          expect(response).to render_template("edit")
        end

        it "should have flash message" do
          attraction = FactoryGirl.create(:attraction, user: user)
          put :update, {:id => attraction.to_param, :attraction => invalid_attributes}, valid_session
          expect(flash[:toast]).to be_present
          expect(flash[:toast]).to eq("Houve problemas ao editar a atração.")
        end
      end
    end

    describe "DELETE destroy" do
      it "destroys the requested attraction" do
        attraction = FactoryGirl.create(:attraction, user: user)
        expect {
          delete :destroy, {:id => attraction.to_param}, valid_session
        }.to change(Attraction, :count).by(-1)
      end

      it "redirects to the attractions list" do
        attraction = FactoryGirl.create(:attraction, user: user)
        delete :destroy, {:id => attraction.to_param}, valid_session
        expect(response).to redirect_to(attractions_path)
      end

      it "should have flash message" do
        attraction = FactoryGirl.create(:attraction, user: user)
        delete :destroy, {:id => attraction.to_param}, valid_session
        expect(flash[:toast]).to be_present
        expect(flash[:toast]).to eq("Atração removida com sucesso!")
      end
    end
  end

end
