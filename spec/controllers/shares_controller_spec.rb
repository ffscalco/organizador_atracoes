require 'rails_helper'

RSpec.describe SharesController, :type => :controller do
  let(:user) {FactoryGirl.create(:user)}

  context "when not logged in" do
    it "should redirect to new user session when try to access index" do
      get :index
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should redirect to new user session when try to access send_attractions" do
      post :send_attractions
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  context "when logged in" do
    before :each do
      sign_in(user)
    end
    let(:attraction) {FactoryGirl.create(:attraction, user: user)}

    let(:valid_params) {
      {
        user_ids: [user.id],
        attraction_ids: [attraction.id]
      }
    }

    describe "get #index" do
      it "should set users unless current" do
        other_user = FactoryGirl.create(:user, email: "other_email@test.com")
        get :index

        expect(User.all.size).to eq(2)
        expect(assigns(:users)).to eq([other_user])
      end

      it "should set attractions of the current user" do
        attraction
        other_user = FactoryGirl.create(:user, email: "other_email@test.com")
        other_attraction = FactoryGirl.create(:attraction, user: user)
        FactoryGirl.create(:attraction, user: other_user)
        get :index

        expect(Attraction.all.size).to eq(3)
        expect(assigns(:attractions).size).to eq(2)
      end
    end

    describe "testing params and #send_attractions" do
      it "should render index with flash if object not exist" do
        post :send_attractions, {}
        expect(response).to render_template("index")

        expect(flash[:toast]).to be_present
        expect(flash[:toast]).to eq("Selecione pelo menos uma atração e um usuário para compartilhar")
      end

      it "should render index with flash if attraction_ids is missing" do
        post :send_attractions, {share:{user_ids:[user.id]}}
        expect(response).to render_template("index")

        expect(flash[:toast]).to be_present
        expect(flash[:toast]).to eq("Selecione pelo menos uma atração para compartilhar")
      end

      it "should render index with flash if user_ids is missing" do
        post :send_attractions, {share:{attraction_ids:[attraction.id]}}
        expect(response).to render_template("index")

        expect(flash[:toast]).to be_present
        expect(flash[:toast]).to eq("Selecione pelo menos um usuário para compartilhar")
      end

      it "should redirect to index with flash if params is ok" do
        post :send_attractions, {share:{attraction_ids:[attraction.id], user_ids:[user.id]}}
        expect(response).to redirect_to(shares_path)

        expect(flash[:toast]).to be_present
        expect(flash[:toast]).to eq("Compartilhado com sucesso!")
      end


      it "should require only 'user_ids' and 'attraction_ids'" do
        self.controller.params[:share] = valid_params.merge!(something: "whatever")
        expect(self.controller.send(:share_params)).to eq("attraction_ids"=>[attraction.id],"user_ids"=>[user.id])
      end
    end
  end
end
