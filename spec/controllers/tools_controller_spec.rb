require 'rails_helper'

RSpec.describe ToolsController, :type => :controller do
  context "when logged in" do
    let(:user) {FactoryGirl.create(:user)}

    describe "and be an admin" do
      let(:admin) {FactoryGirl.create(:user, email: "admin@email.com", admin: true)}
      before :each do
        sign_in(admin)
      end
      it "should access user_attractions" do
        user
        FactoryGirl.create(:user, email: "some@another.com")

        get :user_attractions
        expect(assigns(:users).size).to eq(2)
      end

      describe "get #fetch_attractions" do
        it "should assign user and attractions" do
          3.times {FactoryGirl.create(:attraction, user: user)}
          3.times {FactoryGirl.create(:attraction, user: admin)}
          xhr :get, :fetch_attractions, user_id: user.id

          expect(assigns(:user)).to eq(user)
          expect(Attraction.all.size).to eq(6)
          expect(assigns(:attractions).size).to eq(3)
        end
      end
    end

    describe "rescue_from exceptions" do
      before :each do
        sign_in(user)
      end
      it "rescues from CanCan::AccessDenied" do
        get :user_attractions
        expect(response).to redirect_to(attractions_path)

        expect(flash[:toast]).to be_present
        expect(flash[:toast]).to eq("Você não tem permissão para acessar =|")
      end
    end
  end
end
