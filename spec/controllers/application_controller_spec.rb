require 'rails_helper'

RSpec.describe ApplicationController, :type => :controller do
  describe "#after_sign_in_path_for" do
    it "should redirect to attractions list when loggin" do
      user = FactoryGirl.create(:user)

      expect(controller.after_sign_in_path_for(user)).to eq(attractions_path)
    end

    it "should show toast if has attraction today" do
      user = FactoryGirl.create(:user)
      attraction = FactoryGirl.create(:attraction, user: user, due_date: "2015-01-27", due_time: "08:30:00")
      time_now = Time.parse("2015-01-27 07:30")
      allow(Time).to receive(:now).and_return(time_now)

      sign_in(user)
      controller.after_sign_in_path_for(user)

      expect(flash[:toast]).to be_present
      expect(flash[:toast]).to eq("Sua próxima atração hoje é às 08:30")
    end

    it "should not show toast if hasn't attraction today" do
      user = FactoryGirl.create(:user)
      attraction = FactoryGirl.create(:attraction, user: user, due_date: "2015-01-26", due_time: "06:30:00")
      time_now = Time.parse("2015-01-27 07:30")
      allow(Time).to receive(:now).and_return(time_now)

      sign_in(user)
      controller.after_sign_in_path_for(user)

      expect(flash[:toast]).to be_nil
    end
  end
end
