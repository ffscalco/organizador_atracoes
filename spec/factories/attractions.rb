FactoryGirl.define do
  factory :attraction do
    name "MyString"
    midia "MyString"
    due_date "2015-01-26 00:42:24"
    due_time "00:42:24"
    before(:create) do |attraction, evaluator|
      attraction.user = evaluator.user || FactoryGirl.create(:user)
    end
  end
end
