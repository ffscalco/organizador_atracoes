Rails.application.routes.draw do
  resources :attractions, except: [:show]
  resources :shares, only: [:index] do
    collection do
      match 'send_attractions' => 'shares#send_attractions', via: [:post], as: :send_attractions
    end
  end

  devise_for :users, skip: [:registrations]

  as :user do
    match '/users/sign_up' => 'devise/registrations#new', via: :get, as: :new_user_registration
    match '/users' => 'devise/registrations#create', via: :post, as: :user_registration
  end

  root to: "home#index"

  resources :tools, only: [] do
    collection do
      match 'user_attractions' => 'tools#user_attractions', via: :get, as: :user_attractions
      match 'fetch_attractions' => 'tools#fetch_attractions', via: :get, as: :fetch_attractions
    end
  end
end
