Organização de atrações
---------------

[ ![Codeship Status for ffscalco/organizador_atracoes](https://codeship.com/projects/8a38fd70-8844-0132-e94d-42354043e837/status?branch=master)](https://codeship.com/projects/59371)

Esse aplicativo tem como objetivo gerenciar programas de tv/séries/filmes.

**Requisitos:**

  - O aplicativo deve ser criado nas versões mais recentes do Ruby e do Rails.
  - Usar RVM
  - Usar Rspec e Capybara
  - Usar SASS
  - Fazer os testes unitários, de controladores, de integração e aceitação.
  - Ao terminar subir o projeto no Heroku

**Tarefas:**

  - Criar uma feature para cadastrar, atualizar e apagar as atrações. Quero ver o nome da atração, a mídia na qual assistirei e o horário que será exibido(esse item não precisa ser obrigatório).
  - Também quero ficar sabendo as próximas atrações que assistirei. Preciso que elas sejam ordenadas de acordo com a data de exibição, as mais próximas do horário atual primeiro.
  - Essa aplicação pode ser usada por vários usuários e cada usuário poderá cadastrar e visualizar apenas seus programas.
  - Quero ter 2 níveis de permissão: usuários que podem cadastrar e visualizar apenas suas tarefas e um superadmin que pode acessar qualquer página do sistema.
  - Também quero compartilhar uma atração com outros usuários cadastrados no app. Especificar explicitamente com quais usuários compartilhar.
  - Quero ter uma página que liste os usuários e ao clicar no usuário verei um link com o título de "Minhas atrações". Ao clicar nesse link, abre uma div na mesma página com as atrações cadastradas por esse usuário. Há algumas formas de fazer isso, porém, gostaria que fosse usando o remote: true do Rails.

Qualquer dúvida em relação ao projeto, pode me chamar. A gente vai trabalhar junto então essa comunicação é importante.

**Caso esse app comece a receber 1000 req/s quais medidas você tomaria, no âmbito do aplicativo, para receber essa carga?**


link: http://desafio-agrid-ffscalco.herokuapp.com/

Usuário admin

ffscalco28@gmail.com
super123456
