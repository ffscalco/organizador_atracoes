class ShareMailer < ActionMailer::Base
  default from: "ffscalco28@gmail.com"

  def share(user_id, attraction_ids)
    @user = User.find(user_id)
    @attractions = Attraction.where("id in (?)", attraction_ids)
    mail(to: @user.email, subject: "Novas atrações foram compartilhadas com você")
  end
end
