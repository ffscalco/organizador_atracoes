module ApplicationHelper
  def show_error_messages(errors)
    message = ""

    errors.each do |error|
      message << error + ", "
    end

    return message[0.. message.length-3]
  end

  def set_classes_for_input(invalid)
    return invalid ? "validate invalid" : "validate"
  end
end
