class ToolsController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource :class => ToolsController

  def user_attractions
    @users = User.where("id != ?", current_user.id)
  end

  def fetch_attractions
    @user = User.find(params[:user_id])
    @attractions = Attraction.find_last(@user)
    respond_to do |format|
        format.js
    end
  end
end
