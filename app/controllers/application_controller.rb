class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource)
    attraction = Attraction.find_last(current_user).first
    if !attraction.nil? and attraction.merge_date_time.strftime("%Y-%m-%d %H:%M") > Time.now.strftime("%Y-%m-%d %H:%M")
      flash[:toast] = "Sua próxima atração hoje é às #{attraction.due_time.strftime("%H:%M")}"
    end

    attractions_path
  end

  rescue_from CanCan::AccessDenied do |exception|
    flash[:toast] = exception.message
    redirect_to attractions_path
  end
end
