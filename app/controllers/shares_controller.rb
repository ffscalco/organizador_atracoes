class SharesController < ApplicationController
  before_filter :authenticate_user!

  respond_to :html

  def index
    @users = User.where("id != ?", current_user.id)
    @attractions = Attraction.find_last(current_user)
  end

  def send_attractions
    if params[:share].nil?
      flash[:toast] = "Selecione pelo menos uma atração e um usuário para compartilhar"
      return render :index
    end

    send_params = share_params
    if send_params[:attraction_ids].nil?
      flash[:toast] = "Selecione pelo menos uma atração para compartilhar"
      return render :index
    elsif send_params[:user_ids].nil?
      flash[:toast] = "Selecione pelo menos um usuário para compartilhar"
      return render :index
    end

    send_params[:user_ids].each do |user_id|
      ShareMailer.share(user_id, send_params[:attraction_ids]).deliver_now
    end

    flash[:toast] = "Compartilhado com sucesso!"
    redirect_to shares_path
  end

  private
    def share_params
      params[:share][:attraction_ids] = params[:share][:attraction_ids].flatten.uniq if !params[:share][:attraction_ids].nil?
      params[:share][:user_ids] = params[:share][:user_ids].flatten.uniq if !params[:share][:user_ids].nil?
      params.require(:share).permit(attraction_ids: [], user_ids: [])
    end
end
