class AttractionsController < ApplicationController
  before_action :set_attraction, only: [:edit, :update, :destroy]
  before_filter :authenticate_user!

  respond_to :html

  def index
    @attractions = Attraction.find_last(current_user)
    respond_with(@attractions)
  end

  def new
    @attraction = Attraction.new
    respond_with(@attraction)
  end

  def edit
  end

  def create
    @attraction = Attraction.new(attraction_params)
    @attraction.user = current_user

    if @attraction.save
      flash[:toast] = "Atração criada com sucesso!"
      redirect_to attractions_path
    else
      flash[:toast] = "Houve problemas ao criar uma nova atração."
      render :new
    end
  end

  def update
    if @attraction.update(attraction_params)
      flash[:toast] = "Atração atualizada com sucesso!"
      redirect_to attractions_path
    else
      flash[:toast] = "Houve problemas ao editar a atração."
      render :edit
    end
  end

  def destroy
    @attraction.destroy

    flash[:toast] = "Atração removida com sucesso!"

    redirect_to attractions_path
  end

  private
    def set_attraction
      @attraction = Attraction.find(params[:id])
    end

    def attraction_params
      params[:attraction][:due_date] = params[:attraction][:due_date_submit] if !params[:attraction][:due_date_submit].nil?
      params.require(:attraction).permit(:name, :midia, :due_date, :due_time)
    end
end
