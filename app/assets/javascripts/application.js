//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require jquery.turbolinks
//= require materialize/dist/js/materialize.min.js
//= require pickadate/lib/compressed/picker.time.js
//= require pickadate/lib/compressed/translations/pt_BR.js
//= require_tree .

$(function() {
    $(".button-collapse").sideNav();
    $('.tooltipped').tooltip();
    $('.datepicker').pickadate();
    $('.timepicker').pickatime({format: "HH:i"});
    $('.collapsible').collapsible();
});
