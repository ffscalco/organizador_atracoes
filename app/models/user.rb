class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :validatable, :trackable

  def admin?
    self.admin
  end

  def username
    self.email[/[^@]+/]
  end
end
