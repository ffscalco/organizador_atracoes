class Ability
  include CanCan::Ability

  def initialize(user = User.new)
    if user.admin?
      can :manage, :all
    else
      can :manage, :attractions
      can :manage, :shares
    end
  end
end
