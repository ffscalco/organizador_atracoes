class Attraction < ActiveRecord::Base
  belongs_to :user

  validates :name, :due_date, :due_time, :user, presence: true

  scope :find_last, ->(user_id) { where(:user_id => user_id).order("due_date desc, due_time desc")}

  def merge_date_time
    return DateTime.new(self.due_date.year, self.due_date.month,
      self.due_date.day, self.due_time.hour, self.due_time.min,
      self.due_time.sec, self.due_time.zone)
  end
end
