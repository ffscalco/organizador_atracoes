namespace :user do
  task :create_admin, [ :email, :password ] => :environment do |t, args|
    desc "Task para criar usuário admin"
    User.create(email: args.email, password: args.password, admin: true)
    puts 'lib/tasks/user.rake: create_admin | success!'
  end
end
