class CreateAttractions < ActiveRecord::Migration
  def change
    create_table :attractions do |t|
      t.string :name
      t.string :midia
      t.date :due_date
      t.time :due_time
      t.references :user

      t.timestamps null: false
    end
  end
end
